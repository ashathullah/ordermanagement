package com.mabliz.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * The persistent class for the bank database table.
 * 
 */
@Entity
@Table(name="order_type")
//@NamedQueries({
//@NamedQuery(name="findOrderTypesByRestaurantId", query="SELECT ot FROM OrderType ot WHERE ot.restaurant.id=:restId"),
//@NamedQuery(name="findOrderTypesByRestaurantIdOrderType", query="SELECT ot FROM OrderType ot WHERE ot.restaurant.id=:restId AND ot.id=:typeId"),
//})
public class OrderType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private byte enabled;
	
	@Column(name="order_type")
	private String orderType;

	@Column(name="kot_short_name")
	private String kotShortName;
	
	@Column(name="price_type")
	private long priceType;
	
	@Column(name="kot_printer")
	private long kotPrinter;

	@Column(name="print_token")
	private boolean printToken;

	@Column(name="print_bill")
	private boolean printBill;

	@Column(name="print_recipt")
	private boolean printRecipt;

	@Column(name="print_kot")
	private boolean printKot;

	@Column(name="bill_printer")
	private long billPrinter;
	
	@Column(name="recipt_printer")
	private long reciptPrinter;
	
	@Column(name="token_printer")
	private long tokenPrinter;
	
	@Column(name="group_type")
	private String groupType;
	
	@Column(name="created_by")
	private String createdBy;

	//@Temporal(TemporalType.DATE)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="updated_by")
	private String updatedBy;

	//@Temporal(TemporalType.DATE)
	@Column(name="updated_on")
	private Date updatedOn;
	
	//bi-directional many-to-one association to Restaurant
	// @ManyToOne
	@Column(name="restaurant_id")
	private Long restaurantId;
	
	@Column(name="packing_charge")
	private double packingCharge;
	
	@Column(name="delivery_charge")
	private double deliveryCharge;
	
	@Column(name="tax_calculation",columnDefinition = "varchar(255) default 'Inclusive'")
	private String taxCalculation;
	
	@Column(name="payment_mode",columnDefinition = "varchar(255) default 0")
	private long paymentMode;
	
	@Column(name="round_off",columnDefinition = "int default 1")
	private long roundOff;
	
	@Column(name="cgst",columnDefinition = "double default '0.0'")
	private double cgst;
	
	@Column(name="sgst",columnDefinition = "double default '0.0'")
	private double sgst;

	@Transient
	@ManyToOne
	private OrderTypeAvailability orderTypeAvailability;

	@OneToMany(mappedBy="orderType",fetch = FetchType.LAZY)
	private List<OrderTypePricing> orderTypePricings;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public byte getEnabled() {
		return enabled;
	}

	public void setEnabled(byte enabled) {
		this.enabled = enabled;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getKotShortName() {
		return kotShortName;
	}

	public void setKotShortName(String kotShortName) {
		this.kotShortName = kotShortName;
	}

	public long getPriceType() {
		return priceType;
	}

	public void setPriceType(long priceType) {
		this.priceType = priceType;
	}

	public long getKotPrinter() {
		return kotPrinter;
	}

	public void setKotPrinter(long kotPrinter) {
		this.kotPrinter = kotPrinter;
	}

	public boolean isPrintToken() {
		return printToken;
	}

	public void setPrintToken(boolean printToken) {
		this.printToken = printToken;
	}

	public boolean isPrintBill() {
		return printBill;
	}

	public void setPrintBill(boolean printBill) {
		this.printBill = printBill;
	}

	public boolean isPrintRecipt() {
		return printRecipt;
	}

	public void setPrintRecipt(boolean printRecipt) {
		this.printRecipt = printRecipt;
	}

	public boolean isPrintKot() {
		return printKot;
	}

	public void setPrintKot(boolean printKot) {
		this.printKot = printKot;
	}

	public long getBillPrinter() {
		return billPrinter;
	}

	public void setBillPrinter(long billPrinter) {
		this.billPrinter = billPrinter;
	}

	public long getReciptPrinter() {
		return reciptPrinter;
	}

	public void setReciptPrinter(long reciptPrinter) {
		this.reciptPrinter = reciptPrinter;
	}

	public long getTokenPrinter() {
		return tokenPrinter;
	}

	public void setTokenPrinter(long tokenPrinter) {
		this.tokenPrinter = tokenPrinter;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Long restaurantId) {
		this.restaurantId = restaurantId;
	}

	public double getPackingCharge() {
		return packingCharge;
	}

	public void setPackingCharge(double packingCharge) {
		this.packingCharge = packingCharge;
	}

	public double getDeliveryCharge() {
		return deliveryCharge;
	}

	public void setDeliveryCharge(double deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}

	public String getTaxCalculation() {
		return taxCalculation;
	}

	public void setTaxCalculation(String taxCalculation) {
		this.taxCalculation = taxCalculation;
	}

	public long getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(long paymentMode) {
		this.paymentMode = paymentMode;
	}

	public long getRoundOff() {
		return roundOff;
	}

	public void setRoundOff(long roundOff) {
		this.roundOff = roundOff;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public OrderTypeAvailability getOrderTypeAvailability() {
		return orderTypeAvailability;
	}

	public void setOrderTypeAvailability(OrderTypeAvailability orderTypeAvailability) {
		this.orderTypeAvailability = orderTypeAvailability;
	}

	public List<OrderTypePricing> getOrderTypePricings() {
		return orderTypePricings;
	}

	public void setOrderTypePricings(List<OrderTypePricing> orderTypePricings) {
		this.orderTypePricings = orderTypePricings;
	}

	public boolean getAvailability(){
		if(orderTypeAvailability == null){
			return true;
		}else{
			if(orderTypeAvailability.getAvailable() == 1){
				return true;
			}else {
				return false;
			}
		}
	}

}