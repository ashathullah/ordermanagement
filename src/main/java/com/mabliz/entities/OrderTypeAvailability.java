package com.mabliz.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
// import java.time.LocalDate;
import java.util.Date;

@Entity
public class OrderTypeAvailability implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @Column
    private byte available;

    @Column
    private Date nextAvailable;

    @Column
    private Date updateOn;

    @Column
    private String updatedBy;

    @OneToOne
    @JsonProperty(access= JsonProperty.Access.WRITE_ONLY)
    private OrderType orderType;

    @Transient
    private long orderTypeId;

    @Transient
    private String notes = "";

    public OrderTypeAvailability() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte getAvailable() {
        Date date = new Date();
        if(available == 0){
            if(getNextAvailable() != null && getNextAvailable().after(date)){
                DateTime tomorrow = new DateTime().withTimeAtStartOfDay().plusDays(1);
                DateTime inputTime = new DateTime(getNextAvailable().getTime()).withTimeAtStartOfDay();
                if(tomorrow.equals(inputTime)){
                    notes = "Available Tomorrow "+ new SimpleDateFormat("hh:mm a").format(getNextAvailable());
                }else{
                    notes = "Available "+ new SimpleDateFormat("hh:mm a").format(getNextAvailable());
                }
            }else{
                return 1;
            }
            return 0;
        }
        return 1;
    }

    public void setAvailable(byte available) {
        this.available = available;
    }

    public Date getNextAvailable() {
        return nextAvailable;
    }

    public void setNextAvailable(Date nextAvailable) {
        this.nextAvailable = nextAvailable;
    }

    public Date getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Date updateOn) {
        this.updateOn = updateOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public long getOrderTypeId() {
        if(orderType != null) {
            return orderType.getId();
        }
        return orderTypeId;
    }

    public void setOrderTypeId(long orderTypeId) {
        this.orderTypeId = orderTypeId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
