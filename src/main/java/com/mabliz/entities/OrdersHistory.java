package com.mabliz.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import java.util.Date;


/**
 * The persistent class for the orders_history database table.
 * 
 */
@Entity
@Table(name="orders_history")
@NamedQuery(name="OrdersHistory.findAll", query="SELECT o FROM OrdersHistory o")
public class OrdersHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="bill_amount")
	private double billAmount;

	private String comments;

	@Column(name="created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_updated")
	private Date lastUpdated;

	@Column(name="number_of_customer")
	private int numberOfCustomer;

	@Column(name="razor_status")
	private String razorStatus;
	
	@Column(name="order_status")
	private String orderStatus;

	@Column(name="order_type")
	private long orderType;

	@Column(name="other_notes")
	private String otherNotes;

	private int token;

	@Column(name="updated_by")
	private String updatedBy;

	//bi-directional many-to-one association to Customer
	// @ManyToOne
	// @JsonProperty(access=Access.WRITE_ONLY)
	@Column(name="customer_id")
	private Long customerId;

	//bi-directional many-to-one association to User
	// @ManyToOne
	// @JoinColumn(name="supplier_id")
	// @JsonProperty(access=Access.WRITE_ONLY)
	@Column(name="user_id")
	private Long userId;

	//bi-directional many-to-one association to Order
	@ManyToOne
	@JsonProperty(access=Access.WRITE_ONLY)
	private Order order;

	public OrdersHistory() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getBillAmount() {
		return this.billAmount;
	}

	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public int getNumberOfCustomer() {
		return this.numberOfCustomer;
	}

	public void setNumberOfCustomer(int numberOfCustomer) {
		this.numberOfCustomer = numberOfCustomer;
	}

	public String getOrderStatus() {
		return this.orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public long getOrderType() {
		return this.orderType;
	}

	public void setOrderType(long orderType) {
		this.orderType = orderType;
	}

	public String getOtherNotes() {
		return this.otherNotes;
	}

	public void setOtherNotes(String otherNotes) {
		this.otherNotes = otherNotes;
	}

	public int getToken() {
		return this.token;
	}

	public void setToken(int token) {
		this.token = token;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Order getOrder() {
		return this.order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public String getRazorStatus() {
		return razorStatus;
	}

	public void setRazorStatus(String razorStatus) {
		this.razorStatus = razorStatus;
	}

}