package com.mabliz.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
// import com.fasterxml.jackson.annotation.JsonProperty;
// import com.fasterxml.jackson.annotation.JsonProperty.Access;



/**
 * The persistent class for the order_details database table.
 * 
 */
@Entity
@Table(name="order_details_history")
public class OrderDetailHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="created_by")
	private String createdBy;
	
	//@Temporal(TemporalType.DATE)
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="kot_id")
	private int kotId;

	//@Temporal(TemporalType.DATE)
	@Column(name="last_updated")
	private Date lastUpdated;

	@Column(name = "price")
	private double price;

	@Column(name = "quantity")
	private double quantity;
	
	@Column(name = "status")
	private String status;
		
	@Column(name = "feedback")
	private String feedback;

	@Column(name="table_id")
	private long tableId;
	
    @Column(name="extra_notes")
    private String extraNotes;

	@Column(name="updated_by")
	private String updatedBy;
	
	@Column(name="variant_name")
	private String variantName;

	@Column(name="variant_Id", columnDefinition="default 0")
	private long variantId;
	
	@ManyToOne
	@JsonIgnore
	private OrderDetail orderDetail;

	public OrderDetailHistory() {}
	
	public OrderDetailHistory(OrderDetail od) {
		this.orderDetail = od;
		this.createdBy = od.getCreatedBy();
		this.createdOn= od.getCreatedOn();
		this.extraNotes= od.getExtraNotes();
		this.feedback= od.getFeedback();
		this.kotId= od.getKotId();
		this.lastUpdated= od.getLastUpdated();
		this.price= od.getPrice();
		this.quantity= od.getQuantity();
		this.status= od.getStatus();
		this.tableId= od.getTableId();
		this.updatedBy= od.getUpdatedBy();
		this.lastUpdated= od.getLastUpdated();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public int getKotId() {
		return kotId;
	}

	public void setKotId(int kotId) {
		this.kotId = kotId;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public long getTableId() {
		return tableId;
	}

	public void setTableId(long tableId) {
		this.tableId = tableId;
	}

	public String getExtraNotes() {
		return extraNotes;
	}

	public void setExtraNotes(String extraNotes) {
		this.extraNotes = extraNotes;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OrderDetail getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(OrderDetail orderDetail) {
		this.orderDetail = orderDetail;
	}

	public String getVariantName() {
		return variantName;
	}

	public void setVariantName(String variantName) {
		this.variantName = variantName;
	}

	public long getVariantId() {
		return variantId;
	}

	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}
	
}