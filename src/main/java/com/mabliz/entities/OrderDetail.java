package com.mabliz.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
// import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



/**
 * The persistent class for the order_details database table.
 * 
 */
@Entity
@Table(name="order_details")
//@NamedQueries({
//	@NamedQuery(name="OrderDetail.findAll", query="SELECT o FROM OrderDetail o"),
//	@NamedQuery(name="FindOrderDetailByRestaurantId", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId "),
//	@NamedQuery(name="FindOrderDetailByRestOrderId", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.order.id=:orderId"),
//	@NamedQuery(name="FindOrderDetailByRestKotId", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.kotId=:kotId"),
//	@NamedQuery(name="FindOrderDetailByRestKotIdStatus", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.kotId=:kotId and o.status =:status"),
//	@NamedQuery(name="FindOrderDetailByRestOrderType", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.order.orderType=:orderType"),
//	@NamedQuery(name="FindOrderDetailByRestOrderTypeStatus", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.order.orderType=:orderType and o.status =:status"),
//	@NamedQuery(name="FindOrderDetailByRestTableId", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.tableId=:tableId"),
//	@NamedQuery(name="FindOrderDetailByRestTableIdStatus", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.tableId=:tableId and o.status =:status"),
//	@NamedQuery(name="FindOrderDetailByRestFloorId", query="SELECT o FROM OrderDetail o, FloorTable ft where o.product.restaurant.id=:restId and o.tableId = ft.id and ft.floor.id=:floorId"),
//	@NamedQuery(name="FindOrderDetailByRestFloorIdStatus", query="SELECT o FROM OrderDetail o, FloorTable ft where o.product.restaurant.id=:restId and o.tableId = ft.id and ft.floor.id=:floorId and o.status =:status"),
//	@NamedQuery(name="FindOrderDetailByRestToken", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.order.token =:token"),
//	@NamedQuery(name="FindOrderDetailByRestCustomerId", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.order.customer.id =:customerId"),
//	@NamedQuery(name="FindOrderDetailByRestCreatedBy", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.order.createdBy =:createdBy"),
//	@NamedQuery(name="FindOrderDetailByRestCreatedByStatus", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.order.createdBy =:createdBy and o.status =:status"),
//	@NamedQuery(name="FindOrderDetailByRestUpdatedBy", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.order.updatedBy =:updatedBy"),
//	@NamedQuery(name="FindOrderDetailByRestUpdatedByStatus", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.order.updatedBy =:updatedBy and o.status =:status"),
//	@NamedQuery(name="FindOrderDetailByRestStatus", query="SELECT o FROM OrderDetail o where o.product.restaurant.id=:restId and o.status =:status")
//})
public class OrderDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="created_by")
	private String createdBy;

	//@Temporal(TemporalType.DATE)
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="kot_id")
	private int kotId;

	//@Temporal(TemporalType.DATE)
	@Column(name="last_updated")
	private Date lastUpdated;

	@Column(name="price")
	private double price;
	
	//@Column(name="order_pack_price", columnDefinition="default 0.0")
	@Transient
	private double orderPackPrice=0.0;

	@Column(name="quantity")
	private double quantity;
	
	@Column(name="status")
	private String status;
	
	@Column(name="priority")
	private int priority;
	
	@Column(name="feedback")
	@org.hibernate.annotations.ColumnDefault("null")
	private String feedback;

	@Column(name="table_id")
	private long tableId;

	@Column(name="name")
	private String name;

	@Column(name="variant_name")
	private String variantName;

	@Column(name="variant_Id", columnDefinition="default 0")
	private long variantId;
	
    @Column(name="extra_notes")
    private String extraNotes;

	@Column(name="updated_by")
	private String updatedBy;

	//bi-directional many-to-one association to Order
	@ManyToOne
	@JsonIgnore
	private Order order;


	//bi-directional many-to-one association to Order
	@ManyToOne
	@JoinColumn(name="order_detail_id")
	@JsonIgnore
	private OrderDetail orderDetail;

	@OneToMany(mappedBy="orderDetail")
	private List<OrderDetail> addOns;
	
	//bi-directional many-to-one association to OrderDetail
	@OneToMany(mappedBy="orderDetail")
	@JsonIgnore
	private List<OrderDetailHistory> OrderDetailHistory;

	//bi-directional many-to-one association to Product
	// @ManyToOne
	// @JsonIgnoreProperties(value= {"prices","comboProductMaps1","slaves","addOns2","productQuantity"})
	@Column(name="product_id")
	private Long productId;

	@Transient
	private Double total;
	
	@Transient
	private Double taxableTotal=0.0;

	@Transient
	private String addOnNames;

	public OrderDetail() {
	}

	public OrderDetail getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(OrderDetail orderDetail) {
		this.orderDetail = orderDetail;
	}

	public List<OrderDetail> getAddOns() {
		return addOns;
	}

	public void setAddOns(List<OrderDetail> addOns) {
		this.addOns = addOns;
	}
	
	@JsonIgnore
	public List<com.mabliz.entities.OrderDetailHistory> getOrderDetailHistory() {
		return OrderDetailHistory;
	}

	public void setOrderDetailHistory(List<com.mabliz.entities.OrderDetailHistory> orderDetailHistory) {
		OrderDetailHistory = orderDetailHistory;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public int getKotId() {
		return this.kotId;
	}

	public void setKotId(int kotId) {
		this.kotId = kotId;
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getExtraNotes() {
		return extraNotes;
	}

	public void setExtraNotes(String extraNotes) {
		this.extraNotes = extraNotes;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public double getPrice() {
		/*
		 * if(addOns != null && !addOns.isEmpty()) { double total = price; for
		 * (OrderDetail addOn : addOns) { total = total + addOn.price; } return total; }
		 */
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getQuantity() {
		return this.quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public long getTableId() {
		return this.tableId;
	}

	public void setTableId(long tableId) {
		this.tableId = tableId;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Order getOrder() {
		return this.order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Long getProductId() {
		return this.productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public double getTotal(){
		double total = price * quantity;
		if(addOns != null && !addOns.isEmpty()){
			for (OrderDetail addOn : addOns) {
				total = total + addOn.price * addOn.quantity;
			}
		}
		return total;
	}

	public double getTaxableTotal(){
		/*
		 * double total = 0; if(product.getTaxable() == 1) total = (price * quantity);
		 * if(addOns != null && !addOns.isEmpty()){ for (OrderDetail addOn : addOns) {
		 * if(addOn.product.getTaxable() == 1) total = price * quantity; } }
		 */	
		return taxableTotal;
	}
	
	public void setTaxableTotal(Double taxableTotal) {
		this.taxableTotal = taxableTotal;
	}

	public double getOrderPackPrice() {
		// if(product!=null && product.getPrices()!=null) {
		// 	for(Price price : product.getPrices()) {
		// 		if(order==null || (order.getServiceType()==price.getPriceTypeId() && this.variantId==0) || this.variantId==price.getId()) {
		// 			orderPackPrice = price.getPackingPrice();
		// 			break;
		// 		}
		// 	}
		// 	return orderPackPrice;
		// }else
			return this.orderPackPrice;
	}

	public void setOrderPackPrice(double orderPackPrice) {
		this.orderPackPrice = orderPackPrice;
	}

	// public String getName() {
	// 	if( name == null || name.isEmpty()){
	// 		return product.getDishName();
	// 	}
	// 	return name;
	// }

	public void setName(String name) {
		this.name = name;
	}

	public String getVariantName() {
		return variantName;
	}

	public void setVariantName(String variantName) {
		this.variantName = variantName;
	}

	public long getVariantId() {
		return variantId;
	}

	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}

	public OrderDetail(long id, String createdBy, Date createdOn, int kotId,
					   Date lastUpdated, double price, int quantity, String status,
					   int priority, String feedback, long tableId, String extraNotes,
					   String updatedBy, Order order, Long productId) {
		super();
		this.id = id;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.kotId = kotId;
		this.lastUpdated = lastUpdated;
		this.price = price;
		this.quantity = quantity;
		this.status = status;
		this.priority = priority;
		this.feedback = feedback;
		this.tableId = tableId;
		this.extraNotes = extraNotes;
		this.updatedBy = updatedBy;
		this.order = order;
		this.productId = productId;
	}

}