package com.mabliz.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
// import javax.persistence.JoinColumn;
// import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
// import javax.persistence.OneToOne;
import javax.persistence.Table;
// import javax.persistence.Temporal;
// import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
// import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
// import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the orders database table.
 * 
 */
@Entity
@Table(name="orders")
//@NamedQueries({
//	@NamedQuery(name="Order.findAll", query="SELECT o FROM Order o"),
//	@NamedQuery(name="FindOrderByRestaurantId", query="SELECT o FROM Order o, User u, Restaurant r where r.id=:restId and r.active=1 and o.user.restaurant.id =r.id and r.id=u.restaurant.id and o.user.id= u.id"),
//	@NamedQuery(name="FindOrderByRestOrderId", query="SELECT o FROM Order o where o.user.restaurant.id=:restId and o.id=:orderId and o.user.restaurant.active=1"),
//	@NamedQuery(name="GetLastKotId", query="SELECT o FROM Order o "
//				+" JOIN o.orderDetails od "
//				+" WHERE o.user.restaurant.id=:restId AND o.orderType=:orderType AND DATE(o.createdOn)=CURRENT_DATE"
//				+" GROUP BY o.id,od.kotId"),
//	@NamedQuery(name="FindOrderByRestStatus", query="SELECT o FROM Order o where o.user.restaurant.id=:restId AND (o.orderStatus=:status1 OR o.orderStatus=:status2)"),
//	@NamedQuery(name="FindOrderByRestStatusOrderType", query="SELECT o FROM Order o where o.user.restaurant.id=:restId AND (o.orderStatus=:status1 OR o.orderStatus=:status2) AND o.orderType=:orderType"),
//	@NamedQuery(name="FindOrderByRestStatusCreatedBy", query="SELECT o FROM Order o where o.user.restaurant.id=:restId AND (o.orderStatus=:status1 OR o.orderStatus=:status2) AND o.createdBy=:createdBy"),
//	@NamedQuery(name="FindOrderByRestStatusUpdatedBy", query="SELECT o FROM Order o where o.user.restaurant.id=:restId AND (o.orderStatus=:status1 OR o.orderStatus=:status2) AND o.updatedBy=:updatedBy"),
//	@NamedQuery(name="FindOrderByRestStatusTable", query="SELECT o FROM Order o,FloorTable ft JOIN o.orderDetails od WHERE ft.id=od.tableId AND o.user.restaurant.id=:restId AND (o.orderStatus=:status1 OR o.orderStatus=:status2) AND od.tableId=:tableId  GROUP BY o.id"),
//	@NamedQuery(name="FindOrderByRestStatusFloor", query="SELECT o FROM Order o,FloorTable ft JOIN o.orderDetails od WHERE ft.id=od.tableId AND o.user.restaurant.id=:restId AND (o.orderStatus=:status1 OR o.orderStatus=:status2) AND ft.floor.id=:floorId  GROUP BY o.id"),
//	@NamedQuery(name="FindKotByRestStatus", query="SELECT o,od.kotId FROM Order o "
//			+ "JOIN o.orderDetails od "
//			+ "WHERE  o.user.restaurant.id=:restId AND (od.status=:status1 OR od.status=:status2)  "
//			+ "GROUP BY o.id,od.kotId"),
//	@NamedQuery(name="FindKotByRestStatusToday", query="SELECT o,od.kotId FROM Order o "
//			+ "JOIN o.orderDetails od "
//			+ "WHERE  o.user.restaurant.id=:restId AND (od.status=:status1 OR od.status=:status2)  AND DATE(o.createdOn)=CURRENT_DATE "
//			+ "GROUP BY o.id,od.kotId"),
//	@NamedQuery(name="FindKotByRestOrderId", query="SELECT o,od.kotId FROM Order o "
//			+ "JOIN o.orderDetails od "
//			+ "WHERE o.user.restaurant.id=:restId AND o.id=:orderId "
//			+ "GROUP BY o.id,od.kotId"),
//	@NamedQuery(name="FindKotByRestOrderIdKotId", query="SELECT o,od.kotId FROM Order o "
//			+ "JOIN o.orderDetails od "
//			+ "WHERE o.user.restaurant.id=:restId AND o.id=:orderId AND od.kotId=:kotId "
//			+ "GROUP BY o.id,od.kotId"),
//	@NamedQuery(name="FindByMaxCreatedOnOrders", query="SELECT max(o.createdOn) FROM Order o"),
//	@NamedQuery(name="FindOrderByRestStatusBetweenDates", query="SELECT o FROM Order o WHERE o.user.restaurant.id=:restId "
//			+ " AND (o.createdOn between :startDate and :endDate) AND o.orderStatus = :orderStatus GROUP BY o.createdOn")
//
//	
//})

public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="bill_amount")
	private double billAmount;

	@Column(name="comments")
	private String comments;

	@Column(name="created_by")
	private String createdBy;

	//@Temporal(TemporalType.DATE)
	// @Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on")
	private Date createdOn;
	
	//@Temporal(TemporalType.DATE)
	// @Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_updated")
	private Date lastUpdated;

	@Column(name="number_of_customer")
	private int numberOfCustomer;

	@Column(name="order_status")
	private String orderStatus;
	
	@Column(name="razor_status")
	private String razorStatus;

	@Column(name="service_type")
	private long serviceType;
	
	@Column(name="order_type")
	private long orderType;
	
	@Column(name="order_from")
	private String orderFrom;

	@Column(name="other_notes")
	private String otherNotes;

	@Column(name="token")
	private int token;
	
	@Column(name="updated_by")
	private String updatedBy;
	
//	@Column(name="total_pack_price", columnDefinition="default 0.0")
	@Transient
	private double totalPackPrice=0.0;

	@Transient
	private int isKotPrint;
	
	//bi-directional many-to-one association to Delivery
	@OneToMany(mappedBy="order")
	@JsonIgnore
	private List<Delivery> deliveries;

	//bi-directional many-to-one association to Invoice
	// @OneToOne(mappedBy="order")
	// @JsonManagedReference
	@Column(name="invoice_id")
	private Long invoiceId;
	
	@Column(name="online_id")
	private String onlineId;
	
	//bi-directional many-to-one association to OrderDetail
	@OneToMany(mappedBy="order")
	private List<OrderDetail> orderDetails;

	//bi-directional many-to-one association to Customer
	// @ManyToOne
	@Column(name="customer_id")
	private Long customerId;

	@Transient
	private Integer tableId;

	//bi-directional many-to-one association to User
	// @ManyToOne
	// @JoinColumn(name="supplier_id")
	@Column(name="user_id")
	private Long userId;
	
	//bi-directional many-to-one association to OrdersHistory
	@OneToMany(mappedBy="order")
	@JsonIgnore
	private List<OrdersHistory> ordersHistories;
	
	// @OneToOne
	@Column(name="discount_coupon_log_id")
	private Long discCouponApplyLogId;
	
	public Order() {}

	
	public String getOnlineId() {
		return onlineId;
	}

	public void setOnlineId(String onlineId) {
		this.onlineId = onlineId;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getBillAmount() {
		return this.billAmount;
	}

	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public int getNumberOfCustomer() {
		return this.numberOfCustomer;
	}

	public void setNumberOfCustomer(int numberOfCustomer) {
		this.numberOfCustomer = numberOfCustomer;
	}

	public String getOrderStatus() {
		return this.orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	public long getServiceType() {
		return serviceType;
	}

	public void setServiceType(long serviceType) {
		this.serviceType = serviceType;
	}

	public long getOrderType() {
		return this.orderType;
	}

	public void setOrderType(long orderType) {
		this.orderType = orderType;
	}

	public String getOtherNotes() {
		return this.otherNotes;
	}

	public void setOtherNotes(String otherNotes) {
		this.otherNotes = otherNotes;
	}

	public int getToken() {
		return this.token;
	}

	public void setToken(int token) {
		this.token = token;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public List<Delivery> getDeliveries() {
		return this.deliveries;
	}

	public void setDeliveries(List<Delivery> deliveries) {
		this.deliveries = deliveries;
	}

	public Delivery addDelivery(Delivery delivery) {
		getDeliveries().add(delivery);
		delivery.setOrder(this);

		return delivery;
	}

	public Delivery removeDelivery(Delivery delivery) {
		getDeliveries().remove(delivery);
		delivery.setOrder(null);

		return delivery;
	}

	public Long getInvoiceId() {
		return this.invoiceId;
	}

	public void setInvoices(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public List<OrderDetail> getOrderDetails() {
		return this.orderDetails;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomer(Long customerId) {
		this.customerId = customerId;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUser(Long userId) {
		this.userId = userId;
	}
	
	public List<OrdersHistory> getOrdersHistories() {
		return this.ordersHistories;
	}

	public void setOrdersHistories(List<OrdersHistory> ordersHistories) {
		this.ordersHistories = ordersHistories;
	}

	public long getTableId() {
		if(orderDetails != null && !orderDetails.isEmpty() && orderDetails.get(0) != null){
			return orderDetails.get(0).getTableId();
		}
		return 0L;
	}

	public String getOrderFrom() {
		return orderFrom;
	}


	public void setOrderFrom(String orderFrom) {
		this.orderFrom = orderFrom;
	}

	public double getTotalPackPrice() {
		// if(orderDetails != null && !orderDetails.isEmpty() && orderDetails.get(0) != null){
		// 	for(OrderDetail ordDetail : orderDetails) {
		// 		totalPackPrice = totalPackPrice + (ordDetail.getOrderPackPrice() * ordDetail.getQuantity());
		// 	}
		// }
		return this.totalPackPrice;
	}

	public void setTotalPackPrice(double totalPackPrice) {
		this.totalPackPrice = totalPackPrice;
	}

	public String getRazorStatus() {
		return razorStatus;
	}

	public void setRazorStatus(String razorStatus) {
		this.razorStatus = razorStatus;
	}

	public int getIsKotPrint() {
		return isKotPrint;
	}

	public void setIsKotPrint(int isKotPrint) {
		this.isKotPrint = isKotPrint;
	}

	public Long getDiscCouponApplyLogId() {
		return discCouponApplyLogId;
	}

	public void setDiscCouponApplyLogId(Long discCouponApplyLogId) {
		this.discCouponApplyLogId = discCouponApplyLogId;
	}
	
}