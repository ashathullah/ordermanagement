package com.mabliz.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;



/**
 * The persistent class for the delivery database table.
 * 
 */
@Entity
//@NamedQueries({
//	@NamedQuery(name="Delivery.findAll", query="SELECT d FROM Delivery d"),
//	@NamedQuery(name="Delivery.findDeliveryByOrderRestId", query="SELECT d FROM Delivery d where d.order.id=:orderId and d.order.user.restaurant.id=:restId"),
//	@NamedQuery(name="Delivery.findDeliveryByDeliveryRestId", query="SELECT d FROM Delivery d where d.id=:deliveryId and d.order.user.restaurant.id=:restId"),
//	@NamedQuery(name="Delivery.findDeliveryByOrderId", query="SELECT d FROM Delivery d where d.order.id=:orderId"),
//	@NamedQuery(name="Delivery.findDeliveryByRestId", query="SELECT d FROM Delivery d where d.order.user.restaurant.id=:restId")
//})
public class Delivery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="delivery_user_id")
	private int deliveryUserId;

	@Column(name="is_delivered")
	private byte isDelivered;

	//bi-directional many-to-one association to Order
	@ManyToOne
	private Order order;

	public Delivery() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getDeliveryUserId() {
		return this.deliveryUserId;
	}

	public void setDeliveryUserId(int deliveryUserId) {
		this.deliveryUserId = deliveryUserId;
	}

	public byte getIsDelivered() {
		return this.isDelivered;
	}

	public void setIsDelivered(byte isDelivered) {
		this.isDelivered = isDelivered;
	}

	public Order getOrder() {
		return this.order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

}