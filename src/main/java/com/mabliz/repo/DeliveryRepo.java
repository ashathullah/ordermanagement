package com.mabliz.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabliz.entities.Delivery;

public interface DeliveryRepo extends PagingAndSortingRepository<Delivery, Long>{

}
