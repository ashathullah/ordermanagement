package com.mabliz.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
// import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.mabliz.entities.OrderType;
// import restpick.entities.PrinterData;

public interface OrderTypeRepo extends JpaRepository<OrderType, Long> {

	public List<OrderType> findByRestaurantId(@Param("resId") long resId);
	
	public OrderType findByPriceType(@Param("priceTypeId") long priceTypeId);

	@Query("SELECT o FROM OrderType o WHERE o.id = :id")
	public OrderType findOne(@Param("id") long id);

}
