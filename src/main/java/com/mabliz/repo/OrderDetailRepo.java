package com.mabliz.repo;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

// import com.mabliz.entities.Invoice;
import com.mabliz.entities.OrderDetail;

public interface OrderDetailRepo extends PagingAndSortingRepository<OrderDetail, Long> {

	public List<OrderDetail> findByOrderId(@Param("orderId") long orderId);

}
