package com.mabliz.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mabliz.entities.Order;

public interface OrderRepo extends JpaRepository<Order, Long> {

    @Query("SELECT o FROM Order o WHERE o.id = :id")
    public Order findOne(@Param("id") long id);
    
    
    public List<Order> findByUser_Restaurant_IdAndUser_UserId(@Param("restaurantId") long restaurantId, @Param("userId") String userId);
}
